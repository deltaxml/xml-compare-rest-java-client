
// Copyright 2018 DeltaXML Ltd.  All rights reserved.
// When this source code is provided as part of a DeltaXML product, then the following patents apply to that product: see deltaxml.com/patents

import io.jsonwebtoken.Jwts;
import net.sf.saxon.s9api.*;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An example of a client using the comparison REST API at a low level. Low means that requests/responses are handled as XML
 * Strings (and processed using XPath queries). A higher level interface would use swagger and create programming language
 * classes/objects corresponding to the requests and responses.
 *
 * This example is designed to provide similar capabilities to that of the XML Compare command-line driver (command.jar)
 * command.jar interacts via the Java API while this code provides similar capabilities using calls to the rest server.
 *
 * The REST server must be running for this code to operate. It connects to localhost for the REST service, edit and rebuild to
 * use a different target.
 *
 * This code does not have good error handling. It is not designed to be a production replacement of command.jar (yet), but future
 * releases are expected to improve error handling and reporting.
 */
public class Main {

  private static final String ERROR_RED= "\u001B[31m";
  private static final String ERROR_RESET= "\u001B[0m";

  /**
   * A list of job states. Used to fill in any gaps when the job status is polled in a loop (typically per second).
   */
  private static List<String> stateList= new ArrayList<String>() {
    {
      add("QUEUED");
      add("SUBMITTED");
      add("STARTED");
      add("INPUTS_LOADING_A");
      add("INPUTS_LOADING_B");
      add("INPUT_FILTER_CHAIN_A");
      add("INPUT_FILTER_CHAIN_B");
      add("COMPARISON_RUNNING");
      add("OUTPUT_FILTERS");
      add("SAVING");
      add("FINISHED");
    }
  };

  private static File tokenFile= new File(System.getProperty("user.home"), ".deltaxml-rest-client-token");
  private static List<String> allParametersList, allPipelinesList;
  private static String host;
  private static Client client;
  private static WebTarget target;
  private static Processor p;

  public static void main(String[] args) throws Exception {
    host= "http://localhost:8080";
    client= ClientBuilder.newBuilder().build();
    target= client.target(host + "/api/xml-compare/v1/");
    p= new Processor(false);

    if (System.getProperty("host") != null) {
      host= System.getProperty("host");
    }

    System.out.println("\nDeltaXML XML Compare REST driver. (C) 2019 DeltaXML Ltd.  All rights reserved.");

    if (tokenFile.exists()) {
      try {
        // we need a better of way of doing this. Ideally with sign key.
        // Jws<Claims> jws = Jwts.parser().setSigningKey(NEED SIGN KEY
        // HERE).parseClaimsJws(getToken().substring("Bearer".length()).trim());

        // https://github.com/jwtk/jjwt/issues/67
        String token= getToken().substring("Bearer".length()).trim();
        String withoutSignature= token.substring(0, token.lastIndexOf('.') + 1);
        Jwts.parser().parseClaimsJwt(withoutSignature);
      } catch (io.jsonwebtoken.ExpiredJwtException e) {
        printDescription();
        System.err.printf("\n %-20s\n", "Please authenticate again. " + e.getMessage());
        System.exit(1);
      }
    }

    if (args.length == 0) {
      printDescription();
    } else if (args[0].equals("describe")) {
      if (args.length == 1) {
        printDescription();
        System.err.println(ERROR_RED + "\nERROR: Too few arguments to \'describe\'" + ERROR_RESET);
        System.exit(1);
      }
      String pipelineName= args[1];
      allPipelinesList= getAllPipelineIDs(target, p);
      if (!allPipelinesList.contains(pipelineName)) {
        System.out.println();
        reportPipelines(target, p);
        System.out
            .println(ERROR_RED + "\nERROR: Could not find valid dxp or dcp file with id \'" + pipelineName + "\'" + ERROR_RESET);
        System.exit(1);
      }
      describePipeline(target, p, pipelineName);
    } else if (args[0].equals("authenticate")) {
      generateJwtToken(args[1], args[2], host);
    } else if (args[0].equals("compare")) {
      if (args.length < 5) {
        printDescription();
        System.err.printf(ERROR_RED + "\n %-20s\n", "ERROR: Not enough parameters." + ERROR_RESET);
        System.exit(1);
      }

      String pipelineName= args[1];
      String file1= args[2];
      String file2= args[3];
      String result= args[4];

      allPipelinesList= getAllPipelineIDs(target, p);
      if (!allPipelinesList.contains(pipelineName)) {
        System.out.println();
        reportPipelines(target, p);
        System.out
            .println(ERROR_RED + "\nERROR: Could not find valid dxp or dcp file with id \'" + pipelineName + "\'" + ERROR_RESET);
        System.exit(1);
      }

      allParametersList= getAllPipelineParamaters(target, p, pipelineName);

      Map<String, String> paramSettings= new HashMap<>();
      for (int argc= 5; argc < args.length; argc++) {
        if (args[argc].contains("=")) {
          String[] tokenizedArg= args[argc].split("=");
          if (tokenizedArg.length == 2) {
            paramSettings.put(tokenizedArg[0], tokenizedArg[1]);
          } else {
            System.err.printf(ERROR_RED + "\n %-20s\n", "ERROR: Wrong parameter format: " + args[argc] + ERROR_RESET);
            System.exit(1);
          }
        } else {
          System.err.printf(ERROR_RED + "\n %-20s\n", "ERROR: Params must contain =  : " + args[argc] + ERROR_RESET);
          System.exit(1);
        }
      }

      Response response= getResponse("/pipelines/" + pipelineName, target);
      String delta= response.readEntity(String.class);
      XdmNode n= p.newDocumentBuilder().build(new StreamSource(new StringReader(delta)));

      StringBuffer parameterSettingsAsXml= new StringBuffer();
      for (Map.Entry<String, String> e : paramSettings.entrySet()) {
        String paramType = null;
        String paramName = e.getKey();
        String paramValue = e.getValue();

        if (allParametersList.contains(paramName)) {
          paramType= getStringByXPath(p, n, String
              .format("/pipeline/configurationParameters/configurationParameter[name='%s']/@type", paramName));

          if (paramType.equals("boolean") && !(paramValue.equals("true") || paramValue.equals("false"))) {
            System.err.printf(ERROR_RED + "\nERROR: Parameter \"%s\" can be either \"true\" or \"false\"\n" + ERROR_RESET, paramName);
            System.exit(1);
          }

        } else {
          describePipeline(target, p, pipelineName);
          System.err.printf(ERROR_RED + "\nERROR: Parameter \"%s\" is not a valid parameter for this pipeline" + ERROR_RESET + "\n", paramName);
          System.exit(1);
        }
        String parameterSettingString = String.format(
                "<comparisonParameter type=\"%s\"><name>%s</name><value>%s</value></comparisonParameter>\n", paramType, e.getKey(), e.getValue());
        parameterSettingsAsXml.append(parameterSettingString);
      }

      File f1= new File(file1);
      File f2= new File(file2);

      if (!f1.exists() && !f2.exists()) {
        printDescription();
        // throw new FileNotFoundException("\n" + ERROR_RED + "Files \n \"" + file1 + "\" \n \"" + file2 + "\" \nnot found!" +
        // ERROR_RESET);
        System.err.print("\n" + ERROR_RED + "ERROR: cannot find input files: " + "\n \"" + file1 + "\" \n \"" + file2 + "\" \n(No such file or directory)" + ERROR_RESET + "\n");
        System.exit(1);
      }
      if (!f1.exists()) {
        printDescription();
        // throw new FileNotFoundException("\n" + ERROR_RED + "File \"" + file1 + "\" not found!" + ERROR_RESET);
        System.err.print("\n" + ERROR_RED + "ERROR: cannot find input file: " + "\"" + file1 + "\" (No such file or directory)" + ERROR_RESET + "\n");
        System.exit(1);
      }
      if (!f2.exists()) {
        printDescription();
        // throw new FileNotFoundException("\n" + ERROR_RED + "File \"" + file2 + "\" not found!" + ERROR_RESET);
        System.err.print("\n" + ERROR_RED + "ERROR: cannot find input file: " + "\"" + file2 + "\" (No such file or directory)" + ERROR_RESET + "\n");
        System.exit(1);
      }

      String comparison= String.format(
                                       "<comparison>\n" +
                                          "<inputA type=\"file\"><path>%s</path></inputA>\n" +
                                           "<inputB type=\"file\"><path>%s</path></inputB>\n" +
                                           "<async>" +
                                               "<output type=\"file\"><path>%s</path></output>" +
                                           "</async>" +
                                           "<configurationParameters>%s</configurationParameters>\n" +
                                       "</comparison>",
                                       f1.getAbsolutePath(), f2.getAbsolutePath(),
                                       new File(result).getAbsolutePath(), parameterSettingsAsXml);

      if (tokenFile.exists()) {
        response= target.path("/pipelines/" + pipelineName)
            .request()
            .header(HttpHeaders.AUTHORIZATION, getToken())
            .accept(MediaType.APPLICATION_XML)
            .post(Entity.entity(comparison, MediaType.APPLICATION_XML_TYPE));
      } else {
        response= target.path("/pipelines/" + pipelineName)
            .request()
            .accept(MediaType.APPLICATION_XML)
            .post(Entity.entity(comparison, MediaType.APPLICATION_XML_TYPE));
      }

      if (response.getStatus() == 401) {
        System.err.printf("\n %-20s\n", "Authorisation failed. Please authenticate before any other requests.");
        printDescription();
        System.exit(1);
      }

      URI jobURI= response.getLocation();
      System.out.println(jobURI);
      String jobSuffix= jobURI.getPath().replace("/api/xml-compare/v1/", "");
      boolean finished= false;
      String previousState= "QUEUED";
      System.out.print("Progress:  ");
      String jobState= null;
      String xmlJobState= null;
      while (!finished) {
        response= getResponse(jobSuffix, target);
        xmlJobState= response.readEntity(String.class);
        XdmNode stateTree= p.newDocumentBuilder().build(new StreamSource(new StringReader(xmlJobState)));
        jobState= getStringByXPath(p, stateTree, "/job/jobStatus");
        finished= "FINISHED".equals(jobState) || "FAILED".equals(jobState);
        if (!jobState.equals(previousState) && !jobState.equals("FAILED")) {
          // Ignore for FAILED state - not in stateList and could happen at any point in cycle
          for (String state : stateList.subList(stateList.indexOf(previousState), stateList.indexOf(jobState))) {
            System.out.print(" " + state);
          }
          if ("FINISHED".equals(jobState)) {
            System.out.print(" " + jobState);// and ideally all states between previous and current!
          }
          previousState= jobState;
        }
        Thread.sleep(1000);
      }
      System.out.println();
      if ("FAILED".equals(jobState)) {
        System.out.println("COMPARISON FAILED");
        XdmNode resultTree= p.newDocumentBuilder().build(new StreamSource(new StringReader(xmlJobState)));
        String errorMessage= getStringByXPath(p, resultTree, "/job/error/errorMessage");
        printDescription();
        System.err.printf("\n %-20s\n", ERROR_RED + "ERROR: " + errorMessage + ERROR_RESET);
      } else {
        System.out.println("Result written to " + args[4]);
        previousState= "QUEUED";
        jobState= null;
        System.exit(0);
      }
    } else {
      printDescription();
      System.err.printf("\n %-20s\n", "Argument '" + args[0] + "' not recognized");
      System.exit(1);
    }
  }

  /**
   * Lists the parameters, their types and default values to System.out
   *
   * @param target client info
   * @param p A Saxon Processor
   * @param pipelineName the pipeline to report
   * @throws SaxonApiException Exception thrown by Saxon
   */
  private static void describePipeline(WebTarget target, Processor p, String pipelineName) throws SaxonApiException {
    System.out.println("\nPipeline ID: " + pipelineName + "\n");
    Response response= getResponse("/pipelines/" + pipelineName, target);
    if (response != null) {
      String delta= response.readEntity(String.class);
      XdmNode n= p.newDocumentBuilder().build(new StreamSource(new StringReader(delta)));
      long paramCount= getLongByXPath(p, n, "count(/pipeline/configurationParameters/*)");

      String shortDescription= getStringByXPath(p, n, "normalize-space(/pipeline/name)");
      String fullDescription= getStringByXPath(p, n, "normalize-space(/pipeline/fullDescription)");
      System.out.println("Short Description:\n" + shortDescription + "\nFull Description:\n" + fullDescription + "\n");

      System.out.println("Pipeline Parameters:");
      System.out.printf(" %-30s | %-9s | %-20s | %s\n", "Name", "Type", "Default Value", "Description");
      System.out.print(String.format(" %-30s + %-9s + %-20s + %-50s\n", "-", "-", "-", "-").replace(" ", "-"));
      for (long l= 1; l <= paramCount; l++) {
        String name= getStringByXPath(p, n, String
            .format("normalize-space(/pipeline/configurationParameters/configurationParameter[%d]/name)", l));
        String defaultValue= getStringByXPath(p, n, String
            .format("normalize-space(/pipeline/configurationParameters/configurationParameter[%d]/defaultValue)", l));
        String type= getStringByXPath(p, n,
                                      String.format("/pipeline/configurationParameters/configurationParameter[%d]/@type", l));
        String description= getStringByXPath(p, n, String
            .format("normalize-space(/pipeline/configurationParameters/configurationParameter[%d]/description)", l));

        System.out.printf(" %-30s | %-9s | %-20s | %s\n", name, type, defaultValue, description);
      }
      System.out.print(String.format(" %-30s + %-9s + %-20s + %-50s\n", "-", "-", "-", "-").replace(" ", "-"));
    }
  }

  private static List<String> getAllPipelineParamaters(WebTarget target, Processor p, String pipelineName)
      throws SaxonApiException {
    List<String> parameterIDs= null;
    Response response= getResponse("/pipelines/" + pipelineName, target);
    if (response != null) {
      parameterIDs= new ArrayList<String>();
      String delta= response.readEntity(String.class);
      XdmNode n= p.newDocumentBuilder().build(new StreamSource(new StringReader(delta)));
      long paramCount= getLongByXPath(p, n, "count(/pipeline/configurationParameters/*)");
      String name;
      for (long l= 1; l <= paramCount; l++) {
        name= getStringByXPath(p, n, String
            .format("normalize-space(/pipeline/configurationParameters/configurationParameter[%d]/name)", l));
        parameterIDs.add(name);
      }
    }
    return parameterIDs;
  }

  /**
   * Lists the pipelines using GET on the /pipelines resource
   *
   * @param target client info
   * @param p A Saxon Processor
   * @throws SaxonApiException Exception thrown by Saxon
   */
  private static void reportPipelines(WebTarget target, Processor p) throws SaxonApiException {
    Response response= getResponse("/pipelines", target);
    if (response != null) {
      String pipelines= response.readEntity(String.class);
      XdmNode n= p.newDocumentBuilder().build(new StreamSource(new StringReader(pipelines)));
      long pipelineCount= getLongByXPath(p, n, "count(//pipeline)");

      System.out.printf(" %-20s | %s\n", "Pipeline ID", "Short Description");
      System.out.print(String.format(" %-20s + %-50s\n", "-", "-").replace(" ", "-"));
      for (long l= 1; l <= pipelineCount; l++) {
        String id= getStringByXPath(p, n, String.format("/pipelines/pipeline[%d]/id", l));
        String name= getStringByXPath(p, n, String.format("/pipelines/pipeline[%d]/name", l));
        System.out.printf(" %-20s | %s\n", id, name);
      }
      System.out.print(String.format(" %-20s + %-50s\n", "-", "-").replace(" ", "-"));
    }
  }

  private static List<String> getAllPipelineIDs(WebTarget target, Processor p) throws SaxonApiException {
    List<String> pipelineIDs= null;
    Response response= getResponse("/pipelines", target);
    if (response != null) {
      pipelineIDs= new ArrayList<>();
      String pipelines= response.readEntity(String.class);
      XdmNode n= p.newDocumentBuilder().build(new StreamSource(new StringReader(pipelines)));
      long pipelineCount= getLongByXPath(p, n, "count(//pipeline)");

      for (long l= 1; l <= pipelineCount; l++) {
        String id= getStringByXPath(p, n, String.format("/pipelines/pipeline[%d]/id", l));
        pipelineIDs.add(id);
      }
    }
    return pipelineIDs;
  }

  /**
   * Gets a value from a node tree using an XPath
   *
   * @param p Saxon processor
   * @param n node tree
   * @param xpath the query
   * @return a string value corresponding to the XPath
   * @throws SaxonApiException Exception thrown by Saxon
   */
  private static String getStringByXPath(Processor p, XdmNode n, String xpath) throws SaxonApiException {

    XPathSelector xps= p.newXPathCompiler().compile(xpath).load();
    xps.setContextItem(n);
    return xps.evaluateSingle().getStringValue();
  }

  private static long getLongByXPath(Processor p, XdmNode n, String xpath) throws SaxonApiException {
    XPathSelector xps= p.newXPathCompiler().compile(xpath).load();
    xps.setContextItem(n);
    return ((XdmAtomicValue)xps.evaluateSingle()).getLongValue();
  }

  private static void generateJwtToken(String username, String password, String host) {
    ClientConfig authorisedClientConfig= new ClientConfig();
    HttpAuthenticationFeature authFeature= HttpAuthenticationFeature.basic(username, password);
    authorisedClientConfig.register(authFeature);
    Client authorisedClient= ClientBuilder.newClient(authorisedClientConfig);
    WebTarget targetForAuthentication= authorisedClient.target(host + "/api/xml-compare/v1/");
    Response response= targetForAuthentication.path("/authenticate").request().get();

    if (response.getStatus() == 401) {
      System.err.printf(ERROR_RED + "\n %-30s\n",
                        "ERROR: Authentication failed. Please provide correct username and password." + ERROR_RESET);
      System.exit(1);
    }

    String token= response.getHeaderString(HttpHeaders.AUTHORIZATION);

    BufferedWriter writer;
    try {
      writer= new BufferedWriter(new FileWriter(tokenFile));
      writer.write(token);
      writer.close();
      if (tokenFile.exists()) {
        System.out.printf("\n %-30s\n", "Authentication successful. Generated token will expire in 12 hours.");
      }
    } catch (IOException e) {
      System.err.printf(ERROR_RED + "\n %-30s\n", "ERROR: io problem: " + e.getMessage() + ERROR_RESET);
      System.exit(1);
    }
  }

  /**
   *
   * @return a token string from .deltaxml-rest-client-token file
   * @throws SaxonApiException
   */
  private static String getToken() throws SaxonApiException {
    String token= "";
    try {
      FileReader fileReader= new FileReader(tokenFile);
      BufferedReader bufferedReader= new BufferedReader(fileReader);
      token= bufferedReader.readLine();
    } catch (FileNotFoundException e) {
      printDescription();
      System.err
          .printf(ERROR_RED + "\n %-30s\n",
                  "FileNotFoundException : .deltaxml-rest-client-token file created while authentication is not available." + ERROR_RESET);
      System.exit(1);
    } catch (IOException e) {
      printDescription();
      System.err.printf(ERROR_RED + "\n %-30s\n", "ERROR: io problem: " + e.getMessage() + ERROR_RESET);
      System.exit(1);
    }
    return token;
  }

  /**
   *
   * @param path request path
   * @param target client info
   * @return Response
   * @throws SaxonApiException
   */
  private static Response getResponse(String path, WebTarget target) throws SaxonApiException {
    Response response= null;
    try {
      if (tokenFile.exists()) {
        response= target.path(path)
            .request()
            .header(HttpHeaders.AUTHORIZATION, getToken())
            .accept(MediaType.APPLICATION_XML)
            .get();
      } else {
        response= target.path(path).request().accept(MediaType.APPLICATION_XML).get();
      }

      if (response.getStatus() == 401) {
        System.err.printf(ERROR_RED + " %-20s\n",
                          "Authorisation failed. Please authenticate before any other requests." + ERROR_RESET);
        System.exit(1);
      }
    } catch (ProcessingException e) {
      System.out.println(ERROR_RED + "ERROR: REST server is not responding" + ERROR_RESET);
      System.exit(1);
    }

    return response;
  }

  private static void printDescription() throws SaxonApiException {
    System.out.println();
    System.out.println("Usage:");
    System.out
        .println("  java -jar restclient.jar                                                              : provides pipeline list");
    System.out
        .println("  java -jar restclient.jar authenticate {username} {password}                           : authenticate redistribution users and get a jwt token");
    System.out
        .println("  java -jar restclient.jar describe {pipeline}                                          : describes parameters");
    System.out
        .println("  java -jar restclient.jar compare {pipeline} f1.xml f2.xml result.xml {param=value}    : runs comparison");
    System.out
        .println("  java -Dhost=http://localhost:1234 -jar restclient.jar                                 : use property \"host\" to change scheme, hostname, or port");
    System.out.println();
    System.out.println();
    reportPipelines(target, p);
  }
}
