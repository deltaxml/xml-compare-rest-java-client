# XML Compare REST JAVA Client
This sample command-line driver can be used to experiment with XML Compare's REST API. Please note that the REST server must be running for this code to operate. See [the setup guide](https://docs.deltaxml.com/xml-compare/latest/xml-compare-rest-service/xml-compare-rest-on-premise-installation-and-setup-guide) for instructions.
This client connects to http://localhost:8080 by default, but you can use the property "host" to customize the host, e.g. -Dhost=http://localhost:1234 to point to the REST service running on a different port to the default of 8080.

This command-line driver was written as proof of concept to demonstrate use of the XML Compare REST API, and is not intended as a primary way of interacting with it. The source code for this command-line driver is available as a Maven project. Please customize as needed. Currently, it uses asynchronous calls with XML request. This sample processes responses as Strings and uses XPath to get information from the responses.

The functionality of the REST command-line driver is similar to our [Java API-based command-line driver](https://docs.deltaxml.com/xml-compare/latest/release-documentation/using-the-command-line-tool).

## List the Available Commands
Remember the REST server must be running for this code to operate.
```
java -jar deltaxml-compare-rest-client-x.y.z.jar
DeltaXML XML Compare REST driver. (C) 2019 DeltaXML Ltd.  All rights reserved.

Usage:
  java -jar restclient.jar                                                              : provides pipeline list
  java -jar restclient.jar authenticate {username} {password}                           : authenticate redistribution users and get a jwt token
  java -jar restclient.jar describe {pipeline}                                          : describes parameters
  java -jar restclient.jar compare {pipeline} f1.xml f2.xml result.xml {param=value}    : runs comparison
  java -Dhost=http://localhost:1234 -jar restclient.jar                                 : use property "host" to change scheme, hostname, or port

 pipeline id          | short description
----------------------+---------------------------------------------------
 schema               | [DXP] Schema Compare, output HTML report
 doc-delta            | [DCP] XML Compare, output XML delta
 diffreport-sbs       | [DXP] XML Compare, output HTML5 Side-by-Side report
 doc-diffreport       | [DCP] XML Compare, output HTML folding report
 doc-diffreport-sbs   | [DCP] XML Compare, output HTML Side-by-Side report
 delta                | [DXP] XML Compare, output XML delta
 diffreport           | [DXP] XML Compare, output HTML folding report
 raw                  | [DXP] XML Compare, output recombinable delta
 xhtml                | [DXP] XHTML Compare, output XHTML
----------------------+---------------------------------------------------	
	
```

## Describe the Pipeline Parameters
This example is for the `delta` pipeline

```
java -jar deltaxml-rest-client-10.0.6.jar describe delta

DeltaXML XML Compare REST driver. (C) 2019 DeltaXML Ltd.  All rights reserved.

Pipeline ID: delta

Short Description:
[DXP] XML Compare, output XML delta
Full Description:
This configuration is used to produce an XML delta file representing changes in the input xml.

Pipeline Parameters:
 Name                           | Type      | Default Value        | Description
--------------------------------+-----------+----------------------+---------------------------------------------------
 Preserve Whitespace            | boolean   | false                | whether to normalise whitespace before comparison
 Full Context                   | boolean   | true                 | whether to include unchanged data in the delta file
 Word By Word                   | boolean   | false                | whether to compare PCDATA in a more detailed way
 Enhanced Match 1               | boolean   | true                 | whether to use the 4.x enhanced matcher that is tailored to comparing documents
 Indent                         | string    | yes                  | whether to pretty print the output or not. May be set to 'yes' or 'no'
--------------------------------+-----------+----------------------+---------------------------------------------------

```

## Run a Comparison
```
java -jar deltaxml-rest-client-10.0.6.jar compare delta a.xml b.xml result.xml

DeltaXML XML Compare REST driver. (C) 2019 DeltaXML Ltd.  All rights reserved.
http://localhost:8080/api/xml-compare/v1/jobs/973663c7-4d86-4831-8536-a67301788781
Progress:   QUEUED SUBMITTED STARTED INPUTS_LOADING_A INPUTS_LOADING_B INPUT_FILTER_CHAIN_A INPUT_FILTER_CHAIN_B COMPARISON_RUNNING OUTPUT_FILTERS SAVING FINISHED
Result written to result.xml


```
